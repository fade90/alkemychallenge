import { useParams, useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import './post.css';

import { useAppContext } from '../../AppContext';

const Post = () => {
  const history = useHistory();
  const { posts } = useAppContext();
  const { postId } = useParams();

  const [post, setPost] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const findPost = posts.find(({ id }) => id == postId);

    if (findPost) return setPost(findPost);

    if (!findPost) {
      axios
        .get(`https://jsonplaceholder.typicode.com/posts/${postId}`)
        .then((response) => {
          setPost(response.data);
        })
        .catch((error) => {
          setError(error);
        });
    }
  }, [postId]);

  if (error) {
    return (
      <div className="ui massive container negative message">
        <p>404 error. This page doesn't exist</p>
      </div>
    );
  }

  return (
    <div className="ui container">
      <div className="margin ui container piled segment">
        <h4 className="ui header">Post Content</h4>
        <p>{post.body}</p>
      </div>
      <button type="button" onClick={() => history.goBack()} className="ui button">
        Go Back
      </button>
    </div>
  );
};

export default Post;
