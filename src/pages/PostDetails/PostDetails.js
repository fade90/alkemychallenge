import { useState } from 'react';
import axios from 'axios';
import { useLocation, useHistory } from 'react-router-dom';
import { useAppContext } from '../../AppContext';

const PostDetails = () => {
  const history = useHistory();
  const { setPosts } = useAppContext();
  const { post } = useLocation();
  const [data, setData] = useState({ title: post ? post.title : '', body: post ? post.body : '' });

  const changeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const isValidForm = () => data.title && data.body;

  const submitHandler = (e) => {
    e.preventDefault();

    if (post) {
      axios
        .put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, data)
        .then((response) => {
          setPosts((prevPosts) => {
            const prevPostIdx = prevPosts.findIndex((p) => p.id === post.id);
            prevPosts[prevPostIdx] = response.data;
            return [...prevPosts];
          });
          history.push('/');
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      axios
        .post('https://jsonplaceholder.typicode.com/posts', data)
        .then((response) => {
          setPosts((prevPosts) => [response.data, ...prevPosts]);
          history.push('/');
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  return (
    <div className="ui container">
      <form className="ui large form" onSubmit={submitHandler}>
        <div className="field">
          <label>Post Title</label>
          <div className="ui fluid input">
            <input required type="text" name="title" value={data.title} onChange={changeHandler} />
          </div>
        </div>
        <div className="field">
          <label>Post Content</label>
          <div className="ui fluid input">
            <textarea required type="text" name="body" value={data.body} onChange={changeHandler} />
          </div>
        </div>
        <button type="button" onClick={() => history.goBack()} className="ui button">
          Go Back
        </button>
        <button
          type="submit"
          onClick={submitHandler}
          className="ui primary button"
          disabled={!isValidForm()}>
          Save
        </button>
      </form>
    </div>
  );
};
export default PostDetails;
