import { Link } from 'react-router-dom';
import axios from 'axios';
import './postsList.css';

import { useAppContext } from '../../AppContext';

const PostsList = () => {
  const { posts, setPosts } = useAppContext();

  const deletePost = (id) => {
    axios
      .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        setPosts(posts.filter((item) => item.id !== id));
      })
      .catch((err) => console.log(err));
  };

  const confirmDelete = (id) => {
    if (window.confirm('Are you Sure?')) {
      deletePost(id);
    }
  };

  const result = posts.map((post, index) => (
    <div className="ui raised segment grid " key={index}>
      <div className="thirteen wide column">
        <Link to={{ pathname: `/post/${post.id}` }}>
          <p>{post.title}</p>
        </Link>
      </div>
      <div className="three wide column grid">
        <div className="column">
          <button
            data-tooltip="Delete Post"
            data-position="top center"
            className="icon negative ui right floated button mini"
            onClick={() => confirmDelete(post.id)}>
            <i className="icon trash alternate outline"></i>
          </button>
        </div>
        <Link to={{ pathname: `/edit/${post.id}`, post }}>
          <div className="column">
            <button
              data-tooltip="Edit Post"
              data-position="top center"
              className="icon positive ui right floated button mini">
              <i className="icon edit outline"></i>
            </button>
          </div>
        </Link>
      </div>
    </div>
  ));

  return <div className="ui container">{result}</div>;
};

export default PostsList;
