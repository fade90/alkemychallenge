import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <div className="ui container menu">
      <Link to="/" className="item">
        <h4>Home</h4>
      </Link>
      <Link to="/create" className="item">
        <h4>Create Post</h4>
      </Link>
    </div>
  );
};

export default Header;
