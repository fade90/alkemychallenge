import ReactDOM from 'react-dom';
import App from './App';

import { Provider } from './AppContext';

ReactDOM.render(
  <Provider>
    <App />
  </Provider>,
  document.querySelector('#root')
);
