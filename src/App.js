import { useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { useAppContext } from './AppContext';

import axios from 'axios';
import PostsList from './pages/PostsList';
import Post from './pages/Post';
import PostDetails from './pages/PostDetails';
import Header from './Header/Header';

const App = () => {
  const { posts, setPosts } = useAppContext();

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/posts').then((response) => {
      setPosts(response.data);
    });
  }, [setPosts]);

  return (
    <div>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/">
            <PostsList posts={posts} />
          </Route>
          <Route path="/post/:postId">
            <Post />
          </Route>
          <Route path="/create">
            <PostDetails />
          </Route>
          <Route path="/edit/:postId">
            <PostDetails />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
